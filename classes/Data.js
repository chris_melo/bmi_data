class Data {
    constructor({
       age, gender, p3rd, p5th, p10th, p25th, p50th, p75th, p85th, p90th, p95th, p97th,
    }) {
       
        this.age = age;
        this.gender = gender;
        this.p3rd = p3rd;
        this.p5th = p5th;
        this.p10th = p10th;
        this.p25th = p25th;
        this.p50th = p50th;
        this.p75th = p75th;
        this.p85th = p85th;
        this.p90th = p90th;
        this.p95th = p95th;
        this.p97th = p97th;
    }
   

    async get(pool) {
        const result = await pool.query("SELECT * FROM BMI.DATA_TABLE");
        return result.rows || [];
    } catch (err) {
        console.error('Error executing query:', err);
       
    }
}
    
module.exports = Data;
