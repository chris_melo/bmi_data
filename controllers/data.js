const Data = require("../classes/Data");
 

function render(req,res, data) {
    res.render("pages/index", { 
        data    
        
    });
}

function get(req, res) {
    render(req, res, {});
}

function get(req, res, pool) {
    const dataInstance = new Data({});
    dataInstance.get(pool)
      .then(userData => {
        if (userData.length === 0) {
          return res.render("pages/index", { message: 'No data found.' });
        }
        res.render("pages/index", { data: userData });
      })
      .catch(error => {
        console.error(error);
     
        //res.render("pages/error", { error: 'An error occurred.' });
      });
  }


module.exports = { data_get: get };

