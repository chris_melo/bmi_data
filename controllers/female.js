const Female = require("../classes/Female");
   

function render(req,res, data) {
    res.render("pages/female", { 
        data    
        
    });
}

function get(req, res) {
    render(req, res, {});
}


function get(req, res, pool) {
   
        const dataInstance = new Female({});
        dataInstance.get(pool)
        .then(userData => {
         if (userData.length === 0) {
            return res.render("pages/female", { message: 'No data found.'});
         }
        res.render("pages/female", { data: userData});
    })
    .catch(error => {
      console.error(error);
       //res.render("pages/error", { error: 'An error occurred.' });
    });
}
 
module.exports = { female_get: get };


 




