const Male = require("../classes/Male");
   

function render(req,res, data) {
    res.render("pages/male", { 
        data    
        
    });
}

function get(req, res) {
    render(req, res, {});
}


function get(req, res, pool) {
   
        const dataInstance = new Male({});
        dataInstance.get(pool)
        .then(userData => {
         if (userData.length === 0) {
            return res.render("pages/male", { message: 'No data found.'});
         }
        res.render("pages/male", { data: userData});
    })
    .catch(error => {
      console.error(error);
       //res.render("pages/error", { error: 'An error occurred.' });
    });
}
 
module.exports = { male_get: get };


 




