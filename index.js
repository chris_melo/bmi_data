

const express = require("express");
const app = express();

app.set('view engine', 'ejs');

app.use(express.urlencoded({ extended: true }));
app.use( express.static("./public"));

const appRoutes = require("./routes/routes");
appRoutes(app);






const PORT = process.env.PORT || 3000;

app.listen(PORT, function(err) {
    if(!err) console.log(`Started on port ${PORT}`);
     else console.error(err);
});