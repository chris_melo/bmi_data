const { Pool } = require("pg");
const pool = new Pool({ 
    connectionString: "postgresql://postgres:chrisna@localhost:5432/trivut_demo"
 });


module.exports = function(app) {
   
    const { data_get } = require("../controllers/data");
    app.get("/", async (req, res) => {
        try {
            const data = await data_get(req, res, pool);
          
            //res.render("pages/index", { data});
        } catch (error) {
            console.error('Error rendering view:', error);
            res.status(500).send("Internal Server Error");
           
        }
    });

    const { male_get } = require("../controllers/male");
    app.get("/male", async (req, res) => {
        try {
            const data = await male_get(req, res, pool);
          
            //res.render("pages/male", { data});
        } catch (error) {
            console.error('Error rendering view:', error);
            res.status(500).send("Internal Server Error");
           
        }
    });

    const { female_get } = require("../controllers/female");
    app.get("/female", async (req, res) => {
        try {
            const data = await female_get(req, res, pool);
          
           // res.render("pages/female", { data});
        } catch (error) {
            console.error('Error rendering view:', error);
            res.status(500).send("Internal Server Error");
           
        }
    });
    
}




